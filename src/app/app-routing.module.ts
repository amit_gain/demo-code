import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReservationListComponent } from './reservation/reservation-list/reservation-list.component';
import { ReservationDetailsComponent } from './reservation/reservation-details/reservation-details.component';
import { ReservationResolverService } from './reservation/reservation.services';
import { StayRecordListComponent } from './stay-record/stay-record-list/stay-record-list.component';
import { StayRecordDetailsComponent } from './stay-record/stay-record-details/stay-record-details.component';
import { ProfileListComponent } from './profile/profile-list/profile-list.component';
import { ProfileDetailsComponent } from './profile/profile-details/profile-details.component';
import { GroupBlockListComponent } from './group-block/group-block-list/group-block-list.component';
import { GroupBlockDetailsComponent } from './group-block/group-block-details/group-block-details.component';
import { MessageListComponent } from './message/message-list/message-list.component';
import { MessageDetailsComponent } from './message/message-details/message-details.component';
import { ConfigurationListComponent } from './configuration/configuration-list/configuration-list.component';
import { ConfigurationDetailsComponent } from './configuration/configuration-details/configuration-details.component';
import { StayRecordResolverService } from './stay-record/stay.services';
import { ProfileResolverService } from './profile/profile.services';
import { GroupResolverService } from './group-block/group.services';
import { ConfigurationResolverService } from './configuration/configuration.services';
import { MessageResolverService } from './message/message.services';

const routes: Routes = [
  { path: '', redirectTo: '/reservationlist', pathMatch: 'full' },
  { path: 'reservationlist', component: ReservationListComponent },
  {
    path: 'reservationdetails/:id', component: ReservationDetailsComponent, resolve: {
      reservationDetailData: ReservationResolverService
    }
  },
  { path: 'stayrecordlist', component: StayRecordListComponent },
  {
    path: 'stayrecorddetails/:id', component: StayRecordDetailsComponent, resolve: {
      reservationDetailData: StayRecordResolverService
    }
  },
  { path: 'profilelist', component: ProfileListComponent },
  {
    path: 'profiledetails/:id', component: ProfileDetailsComponent, resolve: {
      reservationDetailData: ProfileResolverService
    }
  },
  { path: 'groupblocklist', component: GroupBlockListComponent },
  {
    path: 'groupblockdetails/:id', component: GroupBlockDetailsComponent, resolve: {
      reservationDetailData: GroupResolverService
    }
  },
  { path: 'messagelist', component: MessageListComponent },
  {
    path: 'messagedetails/:id', component: MessageDetailsComponent, resolve: {
      reservationDetailData: MessageResolverService
    }
  },
  { path: 'configurationlist', component: ConfigurationListComponent },
  {
    path: 'configurationdetails/:id', component: ConfigurationDetailsComponent, resolve: {
      reservationDetailData: ConfigurationResolverService
    }
  },
  { path: '**', component: ReservationListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
