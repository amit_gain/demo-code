import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationResolverService } from '../configuration.services';

@Component({
  selector: 'app-configuration-details',
  templateUrl: './configuration-details.component.html'
})
export class ConfigurationDetailsComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute, private configurationResolverService: ConfigurationResolverService) { }

  revid: string;
  detailReservationData: any;
  linearItemsReservationData: any;
  listValReservationData: any;
  ngOnInit() {
    this.activatedRoute.data.subscribe(res => {
      this.linearItemsReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.linear_items;
      this.listValReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.list_val;
    });
  }

  dialogResponse(type) {
    // console.log('dialogResponse ', type);
    if (type === 'true') {
      this.deleteReservation();
    }

  }
  updateReservation() {
    // console.log('updateReservation: ', this.detailReservationData);
    this.configurationResolverService.updateReservationData(this.detailReservationData).subscribe(res => {
      // console.log('stnc data ', res);

    }
    );
  }
  deleteReservation() {
    // console.log('deleteReservation: ', this.detailReservationData);
    this.configurationResolverService.deleteReservationData(this.detailReservationData).subscribe(res => {
      // console.log('stnc data ', res);

    }
    );
  }
}
