import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { ConfigurationResolverService } from '../configuration.services';

@Component({
  selector: 'app-configuration-list',
  templateUrl: './configuration-list.component.html'
})
export class ConfigurationListComponent implements OnInit {
  public headerList: any;
  public dataList: any;
  public dataListRaw: any;
  public allCheckBoxObj: any;
  public syncDataArray: Array<any>;

  constructor(private configurationResolverService: ConfigurationResolverService) { }

  ngOnInit() {
    this.allCheckBoxObj = {
      checked: false
    };
    this.configurationResolverService.getReservationListData().subscribe(data => {
      if (data.oxi_summary_info_rs) {
        this.headerList = data.oxi_summary_info_rs.headers;
        this.dataList = data.oxi_summary_info_rs.summary_param_val.map(item => {
          return {
            dataModel: item.data,
            checked: false
          };
        });
        this.dataListRaw = JSON.parse(JSON.stringify(this.dataList));
      }
    });
  }

  selectForSync(itemObj, index) {

    if (index === 'all') {
      this.dataList.map(item => {
        item.checked = itemObj.checked;
        return item;
      });
    } else {
      this.allCheckBoxObj.checked = _.every(this.dataList, (item => item.checked === true));
    }

  }

  syncData() {
    this.syncDataArray = [];
    for (const item of this.dataList) {
      if (item.checked === true) {
        this.syncDataArray.push(item.dataModel[1]);
      }
    }
    // console.log('configurationResolverService: ', this.syncDataArray);
  }
}
