import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeUnderscore'
})
export class RemoveUnderscorePipe implements PipeTransform {

  transform(dataList: any): any {
    if (!dataList) {
      return [];
    } else {
      const tempData = dataList.replace(/_/g, ' ');
      return tempData.toLowerCase().split(' ').map((word) => {
        return (word.charAt(0).toUpperCase() + word.slice(1));
      }).join(' ');
    }
  }

}
