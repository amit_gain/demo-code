import * as _ from 'underscore';

export class Utility {
    getDeltaFromNonArrayElement(currentValues, actualValues) {

        const changes = {};
        let prop;
        for (prop in currentValues) {
            if (!actualValues || actualValues[prop] !== currentValues[prop]) {
                if (typeof currentValues[prop] === 'object') {
                    const c = this.getDeltaFromNonArrayElement(currentValues[prop], actualValues[prop]);
                    if (!_.isEmpty(c)) {// underscore
                        changes[prop] = c;
                    }
                } else {
                    changes[prop] = currentValues[prop];
                }
            }
        }
        return changes;
    }
}