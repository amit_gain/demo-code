import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageResolverService } from '../message.services';

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html'
})
export class MessageDetailsComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute, private messageResolverService: MessageResolverService) { }

  revid: string;
  detailReservationData: any;
  linearItemsReservationData: any;
  listValReservationData: any;
  ngOnInit() {
    this.activatedRoute.data.subscribe(res => {
      this.linearItemsReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.linear_items;
      this.listValReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.list_val;
    });
  }

  dialogResponse(type) {
    if (type === 'true') {
      this.deleteReservation();
    }

  }
  updateReservation() {
    this.messageResolverService.updateReservationData(this.detailReservationData).subscribe(res => {
    }
    );
  }
  deleteReservation() {
    this.messageResolverService.deleteReservationData(this.detailReservationData).subscribe(res => {
    }
    );
  }
}
