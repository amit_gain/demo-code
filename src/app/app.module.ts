import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReservationListComponent } from './reservation/reservation-list/reservation-list.component';
import { ReservationDetailsComponent } from './reservation/reservation-details/reservation-details.component';
import { StayRecordListComponent } from './stay-record/stay-record-list/stay-record-list.component';
import { StayRecordDetailsComponent } from './stay-record/stay-record-details/stay-record-details.component';
import { ProfileListComponent } from './profile/profile-list/profile-list.component';
import { ProfileDetailsComponent } from './profile/profile-details/profile-details.component';
import { GroupBlockListComponent } from './group-block/group-block-list/group-block-list.component';
import { GroupBlockDetailsComponent } from './group-block/group-block-details/group-block-details.component';
import { MessageListComponent } from './message/message-list/message-list.component';
import { MessageDetailsComponent } from './message/message-details/message-details.component';
import { ConfigurationListComponent } from './configuration/configuration-list/configuration-list.component';
import { ConfigurationDetailsComponent } from './configuration/configuration-details/configuration-details.component';
import { CustomDialogComponent } from './shared/custom-dialog/custom-dialog.component';
import { RemoveUnderscorePipe } from './shared/filters/remove-underscore.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ReservationListComponent,
    ReservationDetailsComponent,
    StayRecordDetailsComponent,
    StayRecordListComponent,
    ProfileListComponent,
    ProfileDetailsComponent,
    GroupBlockDetailsComponent,
    GroupBlockListComponent,
    MessageListComponent,
    MessageDetailsComponent,
    ConfigurationDetailsComponent,
    ConfigurationListComponent,
    CustomDialogComponent,
    RemoveUnderscorePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
