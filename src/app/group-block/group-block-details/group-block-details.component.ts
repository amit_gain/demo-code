import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupResolverService } from '../group.services';

@Component({
  selector: 'app-group-block-details',
  templateUrl: './group-block-details.component.html'
})
export class GroupBlockDetailsComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute, private groupResolverService: GroupResolverService) { }

  revid: string;
  detailReservationData: any;
  linearItemsReservationData: any;
  listValReservationData: any;
  ngOnInit() {
    this.activatedRoute.data.subscribe(res => {
      this.linearItemsReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.linear_items;
      this.listValReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.list_val;
    });
  }

  dialogResponse(type) {
    if (type === 'true') {
      this.deleteGroup();
    }

  }
  updateGroup() {
    this.groupResolverService.updateGroupData(this.detailReservationData).subscribe(res => {
    }
    );
  }
  deleteGroup() {
    this.groupResolverService.deleteGroupData(this.detailReservationData).subscribe(res => {
    }
    );
  }
}
