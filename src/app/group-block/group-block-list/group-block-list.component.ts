import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { GroupResolverService } from '../group.services';

@Component({
  selector: 'app-group-block-list',
  templateUrl: './group-block-list.component.html'
})
export class GroupBlockListComponent implements OnInit {
  public headerList: any;
  public dataList: any;
  public dataListRaw: any;
  public allCheckBoxObj: any;
  public syncDataArray: Array<any>;

  constructor(private groupResolverService: GroupResolverService) { }

  ngOnInit() {
    this.allCheckBoxObj = {
      checked: false
    };
    this.groupResolverService.getGroupListData().subscribe(data => {
      if (data.oxi_summary_info_rs) {
        this.headerList = data.oxi_summary_info_rs.headers;
        this.dataList = data.oxi_summary_info_rs.summary_param_val.map(item => {
          return {
            dataModel: item.data,
            checked: false
          };
        });
        this.dataListRaw = JSON.parse(JSON.stringify(this.dataList));
      }
    });
  }

  selectForSync(itemObj, index) {

    if (index === 'all') {
      this.dataList.map(item => {
        item.checked = itemObj.checked;
        return item;
      });
    } else {
      this.allCheckBoxObj.checked = _.every(this.dataList, (item => item.checked === true));
    }

  }

  syncData() {
    this.syncDataArray = [];
    for (const item of this.dataList) {
      if (item.checked === true) {
        this.syncDataArray.push(
          {
            rt_block_inv_id: item.dataModel[0]
          }
        );
      }
    }
    this.groupResolverService.syncGroupData(this.syncDataArray).subscribe(res => {
      if (res.update_block_rs.status === 'Success') {
        const x = document.getElementById('snackbar');
        x.className = 'show';
        setTimeout(() => { x.className = x.className.replace('show', ''); }, 3000);
      } else {
        const y = document.getElementById('snackbarfail');
        y.className = 'show';
        setTimeout(() => { y.className = y.className.replace('show', ''); }, 3000);
      }
    });
  }
}
