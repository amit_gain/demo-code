import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class GroupResolverService implements Resolve<any> {
    constructor(private httpClient: HttpClient) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const revId = route.params.id;
        const getGroupListDataUrl = environment.baseURL + 'oxiDetailInfoService';
        const getGroupListDataHttpOptions = {
            oxi_details_info_rq: {
                message_type: 'GROUP_BLOCK',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
                id: revId
            }
        };
        return this.httpClient.post(getGroupListDataUrl, getGroupListDataHttpOptions)
            .pipe(catchError((err: HttpErrorResponse) => {
                return throwError(err);
            }));
    }

    getGroupListData(): Observable<any> {
        const getGroupListDataUrl = environment.baseURL + 'oxiSummaryInfoService';
        const getGroupListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'GROUP_BLOCK',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
            }
        };
        return this.httpClient.post(getGroupListDataUrl, getGroupListDataHttpOptions);
    }

    syncGroupData(resIds): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + 'oxiBlockInvSync';
        const getReservationListDataHttpOptions = {
            update_block_inv_rq: {
                header: {
                    request_type: 'UPDATE_BLOCK_INV'
                },
                hotel_code: environment.hotel_code,
                request_date: new Date().toString(),
                req_log_id: '',
                block_inv_details: resIds
            }
        };
        console.log('getReservationListDataHttpOptions ', getReservationListDataHttpOptions);
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }

    updateGroupData(resData): Observable<any> {
        const getGroupListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getGroupListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'GROUP_BLOCK',
                hotel_code: environment.hotel_code
            }
        };
        return this.httpClient.post(getGroupListDataUrl, getGroupListDataHttpOptions);
    }
    deleteGroupData(resData): Observable<any> {
        const getGroupListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getGroupListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'GROUP_BLOCK',
                hotel_code: environment.hotel_code
            }
        };
        return this.httpClient.post(getGroupListDataUrl, getGroupListDataHttpOptions);
    }
}
