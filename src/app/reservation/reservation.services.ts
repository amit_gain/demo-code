import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class ReservationResolverService implements Resolve<any> {

    constructor(private httpClient: HttpClient) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const revId = route.params.id;
        console.log('revId ', revId);
        const getReservationListDataUrl = environment.baseURL + 'oxiDetailInfoService';
        const getReservationListDataHttpOptions = {
            oxi_details_info_rq: {
                message_type: 'RESERVATION',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
                id: revId
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions)
            .pipe(catchError((err: HttpErrorResponse) => {
                return throwError(err);
            }));
    }

    getReservationListData(): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + 'oxiSummaryInfoService';
        const getReservationListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'RESERVATION',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }

    syncReservationData(resIds): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + 'oxiReservationSync';
        const getReservationListDataHttpOptions = {
            update_reservation_rq: {
                header: {
                    request_type: 'UPDATE_RESERVATION'
                },
                hotel_code: environment.hotel_code,
                request_date: new Date().toString(),
                req_log_id: '',
                reservation_details: resIds
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }

    updateReservationData(resData): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getReservationListDataHttpOptions = {
            update_profile_rq: {
                header: {
                    request_type: 'UPDATE_PMS_PROFILE'
                },
                hotel_code: environment.hotel_code,
                request_date: '',
                req_log_id: '',
                profile_details: [
                    {
                        rt_profile_id: 1
                    }
                ]
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }
    deleteReservationData(resData): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getReservationListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'PROFILE',
                hotel_code: '1234',
                ext_hotel_code: 'Test123'
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }
}
