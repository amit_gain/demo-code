import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

import { ReservationResolverService } from '../reservation.services';

@Component({
  selector: 'app-reservation-details',
  templateUrl: './reservation-details.component.html'
})
export class ReservationDetailsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private reservationResolverService: ReservationResolverService) { }

  revid: string;
  detailReservationData: any;
  linearItemsReservationData: any;
  listValReservationData: any;
  ngOnInit() {
    this.activatedRoute.data.subscribe(res => {
      // this.detailReservationData = res.reservationDetailData.param_val;
      console.log('res.oxi_detail_info_rq ', res.reservationDetailData);
      this.linearItemsReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.linear_items;
      this.listValReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.list_val;
      console.log('listValReservationData ', this.listValReservationData);

      // console.log('in reservation details2: linearItemsReservationData', this.linearItemsReservationData,
      //   "listValReservationData ", this.listValReservationData);
    });
  }

  dialogResponse(type) {
    console.log('dialogResponse ', type);
    if (type === 'true') {
      this.deleteReservation();
    }

  }
  updateReservation() {
    console.log('updateReservation: ', this.detailReservationData);
    this.reservationResolverService.updateReservationData(this.detailReservationData).subscribe(res => {
      console.log('stnc data ', res);

    }
    );
  }
  deleteReservation() {
    console.log('deleteReservation: ', this.detailReservationData);
    this.reservationResolverService.deleteReservationData(this.detailReservationData).subscribe(res => {
      console.log('stnc data ', res);

    }
    );
  }

}
