import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileResolverService } from '../profile.services';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html'
})
export class ProfileDetailsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private profileResolverService: ProfileResolverService) { }
  detailReservationData: any;
  linearItemsReservationData: any;
  listValReservationData: any;
  ngOnInit() {
    this.activatedRoute.data.subscribe(res => {
      this.detailReservationData = res.reservationDetailData.oxi_details_info_rs;
      this.linearItemsReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.linear_items;
      this.listValReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.list_val;
    });
  }
  dialogResponse(type) {
    if (type === 'true') {
      this.deleteProfile();
    }

  }
  updateProfile() {
    this.profileResolverService.updateProfileData(this.detailReservationData).subscribe(res => {
      if (res.update_profile_rs.status === 'Success') {
        const x = document.getElementById('snackbar');
        x.className = 'show';
        setTimeout(() => { x.className = x.className.replace('show', ''); }, 3000);
      } else {
        const y = document.getElementById('snackbarfail');
        y.className = 'show';
        setTimeout(() => { y.className = y.className.replace('show', ''); }, 3000);
      }
    }
    );
  }
  deleteProfile() {
    // this.profileResolverService.deleteReservationData(resId).subscribe(res => {
    //   console.log('stnc data ', res);

    // }
    // );
  }

}
