import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';

import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';


@Injectable({
    providedIn: 'root',
})
export class ProfileResolverService implements Resolve<any> {

    constructor(private httpClient: HttpClient) { }
    revId: any;
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        console.log('In resolve service: ', route);
        this.revId = route.params.id;
        const getProfileListDataUrl = environment.baseURL + 'oxiDetailInfoService';
        const getProfileListDataHttpOptions = {
            oxi_details_info_rq: {
                message_type: 'PROFILE',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
                id: this.revId
            }
        };
        return this.httpClient.post(getProfileListDataUrl, getProfileListDataHttpOptions)
            .pipe(catchError((err: HttpErrorResponse) => {
                return throwError(err);
            }));
    }

    getProfileListData(): Observable<any> {
        const getProfileListDataUrl = environment.baseURL + 'oxiSummaryInfoService';
        const getProfileListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'PROFILE',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
            }
        };
        return this.httpClient.post(getProfileListDataUrl, getProfileListDataHttpOptions);
    }

    syncProfileData(resIds): Observable<any> {
        const getProfileListDataUrl = environment.baseURL + 'oxiProfileSync';
        const getProfileListDataHttpOptions = {
            update_profile_rq: {
                header: {
                    request_type: 'UPDATE_PMS_PROFILE'
                },
                hotel_code: environment.hotel_code,
                request_date: new Date().toString(),
                req_log_id: '',
                profile_details: resIds
            }
        };
        return this.httpClient.post(getProfileListDataUrl, getProfileListDataHttpOptions);
    }

    updateProfileData(resData): Observable<any> {
        const getProfileListDataUrl = environment.baseURL + 'oxiProfileUpdate';
        const getProfileListDataHttpOptions = {
            update_profile_rq: {
                header: {
                    request_type: 'UPDATE_PMS_PROFILE'
                },
                hotel_code: environment.hotel_code,
                request_date: new Date().toString(),
                req_log_id: '',
                profile_details: resData,
                rt_profile_id:this.revId
            }
        };
        return this.httpClient.post(getProfileListDataUrl, getProfileListDataHttpOptions);
    }
    deleteProfileData(resData): Observable<any> {
        const getProfileListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getProfileListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'PROFILE',
                hotel_code: environment.hotel_code
            }
        };
        return this.httpClient.post(getProfileListDataUrl, getProfileListDataHttpOptions);
    }
}
