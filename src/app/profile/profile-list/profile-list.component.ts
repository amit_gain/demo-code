import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { ProfileResolverService } from '../profile.services';
import { Utility } from '../../shared/utility/Utility';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html'
})
export class ProfileListComponent implements OnInit {
  public headerList: any;
  public dataList: any;
  public dataListRaw: any;
  public allCheckBoxObj: any;
  public syncDataArray: Array<any>;
  private utility: any;
  public reSyncStatus = true;

  constructor(private profileResolverService: ProfileResolverService) { }

  ngOnInit() {
    this.allCheckBoxObj = {
      checked: false
    };
    this.profileResolverService.getProfileListData().subscribe(data => {
      if (data.oxi_summary_info_rs) {
        this.headerList = data.oxi_summary_info_rs.headers;
        this.dataList = data.oxi_summary_info_rs.summary_param_val.map(item => {
          return {
            dataModel: item.data,
            checked: false
          };
        });
        this.dataListRaw = JSON.parse(JSON.stringify(this.dataList));
        console.log('profileResolverService : ', data.oxi_summary_info_rs, this.dataList);
      }
    });
  }

  selectForSync(itemObj, index) {
    this.utility = new Utility();
    if (index === 'all') {
      this.dataList.map(item => {
        item.checked = itemObj.checked;
        return item;
      });
    } else {
      this.allCheckBoxObj.checked = _.every(this.dataList, (item => item.checked === true));
    }
    const dataListDelta = this.utility.getDeltaFromNonArrayElement(this.dataList, this.dataListRaw);
    if (Object.keys(dataListDelta).length > 0) {
      this.reSyncStatus = false;
    } else {
      this.reSyncStatus = true;
    }
    // console.log('getDeltaFromNonArrayElement',this.dataList,
    //  this.utility.getDeltaFromNonArrayElement(this.dataList, this.dataListRaw)
    //  );

  }

  syncData() {

    this.syncDataArray = [];
    for (const item of this.dataList) {
      if (item.checked === true) {
        this.syncDataArray.push(
          {
            rt_profile_id: item.dataModel[0]
          }
        );
      }
    }
    this.profileResolverService.syncProfileData(this.syncDataArray).subscribe(res => {
      if (res.update_profile_rs.status === 'Success') {
        const x = document.getElementById('snackbar');
        x.className = 'show';
        setTimeout(() => { x.className = x.className.replace('show', ''); }, 3000);
      } else {
        const y = document.getElementById('snackbarfail');
        y.className = 'show';
        setTimeout(() => { y.className = y.className.replace('show', ''); }, 3000);
      }
    });

  }


}
