import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

import { StayRecordResolverService } from '../stay.services';

@Component({
  selector: 'app-stay-record-list',
  templateUrl: './stay-record-list.component.html'
})
export class StayRecordListComponent implements OnInit {

  public headerList: any;
  public dataList: any;
  public dataListRaw: any;
  public allCheckBoxObj: any;
  public syncDataArray: Array<any>;

  constructor(private stayRecordResolverService: StayRecordResolverService) { }

  ngOnInit() {
    this.allCheckBoxObj = {
      checked: false
    };
    this.stayRecordResolverService.getReservationListData().subscribe(data => {
      if (data.oxi_summary_info_rs) {
        this.headerList = data.oxi_summary_info_rs.headers;
        this.dataList = data.oxi_summary_info_rs.summary_param_val.map(item => {
          return {
            dataModel: item.data,
            checked: false
          };
        });
        this.dataListRaw = JSON.parse(JSON.stringify(this.dataList));
      }
    });
  }

  selectForSync(itemObj, index) {

    if (index === 'all') {
      this.dataList.map(item => {
        item.checked = itemObj.checked;
        return item;
      });
    } else {
      this.allCheckBoxObj.checked = _.every(this.dataList, (item => item.checked === true));
    }

  }

  syncData() {
    this.syncDataArray = [];
    for (const item of this.dataList) {
      if (item.checked === true) {
        this.syncDataArray.push(item.dataModel[1]);
      }
    }
    console.log('syncDataArray: ', this.syncDataArray);
    const x = document.getElementById('snackbar');
    x.className = 'show';
    setTimeout(() => { x.className = x.className.replace('show', ''); }, 3000);
  }
}
