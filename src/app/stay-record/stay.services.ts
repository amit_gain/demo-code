import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class StayRecordResolverService implements Resolve<any> {
    constructor(private httpClient: HttpClient) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const revId = route.params.id;
        console.log('revId ', revId);
        const getReservationListDataUrl = environment.baseURL + 'oxiDetailInfoService';
        const getReservationListDataHttpOptions = {
            oxi_details_info_rq: {
                message_type: 'STAY_RECORDS',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
                id: revId// '144318'
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions)
            .pipe(catchError((err: HttpErrorResponse) => {
                return throwError(err);
            }));
    }

    getReservationListData(): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + 'oxiSummaryInfoService';
        const getReservationListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'STAY_RECORDS',
                hotel_code: environment.hotel_code,
                ext_hotel_code: environment.ext_hotel_code,
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }
    syncReservationData(resId): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + '';
        console.log('resId ', resId);

        const getReservationListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'STAY_RECORDS',
                hotel_code: environment.hotel_code
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }

    updateReservationData(resData): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getReservationListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'STAY_RECORDS',
                hotel_code: environment.hotel_code
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }
    deleteReservationData(resData): Observable<any> {
        const getReservationListDataUrl = environment.baseURL + '';
        console.log('resId ', resData);

        const getReservationListDataHttpOptions = {
            oxi_summary_info_rq: {
                message_type: 'STAY_RECORDS',
                hotel_code: environment.hotel_code
            }
        };
        return this.httpClient.post(getReservationListDataUrl, getReservationListDataHttpOptions);
    }
}
