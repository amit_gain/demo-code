import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StayRecordResolverService } from '../stay.services';

@Component({
  selector: 'app-stay-record-details',
  templateUrl: './stay-record-details.component.html'
})
export class StayRecordDetailsComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute, private stayRecordResolverService: StayRecordResolverService) { }

  revid: string;
  detailReservationData: any;
  linearItemsReservationData: any;
  listValReservationData: any;
  ngOnInit() {
    this.activatedRoute.data.subscribe(res => {
      this.linearItemsReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.linear_items;
      this.listValReservationData = res.reservationDetailData.oxi_details_info_rs.detail_param_val.list_val;
    });
  }

  dialogResponse(type) {
    console.log('dialogResponse ', type);
    if (type === 'true') {
      this.deleteReservation();
    }

  }
  updateReservation() {
    console.log('updateReservation: ', this.detailReservationData);
    this.stayRecordResolverService.updateReservationData(this.detailReservationData).subscribe(res => {
      console.log('stnc data ', res);

    }
    );
  }
  deleteReservation() {
    console.log('deleteReservation: ', this.detailReservationData);
    this.stayRecordResolverService.deleteReservationData(this.detailReservationData).subscribe(res => {
      console.log('stnc data ', res);

    }
    );
  }
}
